const chai = require('chai')
process.env.NODE_ENV = 'test'

const chaiHttp = require('chai-http')
const should = chai.should()
const app = require('../../src/app')
const { Order } = require('../../src/domains/order/order.model')

chai.use(chaiHttp)

describe('Orders', () => {
  beforeEach(done => {
    Order.destroy({ where: {} }).then(() => {
      done()
    })
  })

  describe('POST', () => {
    describe('/orders', () => {
      it('should create an order with origin and destination set properly', done => {
        let order = {
          origin: ['52.5160', '13.3779'],
          destination: ['52.5206', '13.3862'],
        }
        chai
          .request(app)
          .post('/orders')
          .send(order)
          .end((err, res) => {
            res.should.have.status(200)
            res.body.should.be.a('object')
            res.body.should.have.property('id').be.a('number')
            res.body.should.have.property('distance').be.a('number')
            res.body.should.have.property('status').eql('UNASSIGNED')
            done()
          })
      })

      it('should fail creating an order when origin only have 1 element', done => {
        let order = {
          origin: ['52.5160'],
          destination: ['52.5206', '13.3862'],
        }
        chai
          .request(app)
          .post('/orders')
          .send(order)
          .end((err, res) => {
            res.should.have.status(400)
            res.body.should.be.a('object')
            res.body.should.have.property('error')
            done()
          })
      })

      it('should fail creating an order when destination is an array of 2 float numbers', done => {
        let order = {
          origin: ['52.5160', '13.3779'],
          destination: [52.5206, 13.3862],
        }
        chai
          .request(app)
          .post('/orders')
          .send(order)
          .end((err, res) => {
            res.should.have.status(400)
            res.body.should.be.a('object')
            res.body.should.have.property('error')
            done()
          })
      })

      it('should fail creating an order when destination is an array of 2 integers', done => {
        let order = {
          origin: ['52.5160', '13.3779'],
          destination: [52, 13],
        }
        chai
          .request(app)
          .post('/orders')
          .send(order)
          .end((err, res) => {
            res.should.have.status(400)
            res.body.should.be.a('object')
            res.body.should.have.property('error')
            done()
          })
      })
    })
  })

  describe('GET', () => {
    describe('/orders?page=1&limit=10', () => {
      it('should GET at most 10 orders', done => {
        chai
          .request(app)
          .get('/orders?page=1&limit=10')
          .end((err, res) => {
            res.should.have.status(200)
            res.body.should.be.a('array')
            res.body.length.should.be.lte(10)
            done()
          })
      })
    })

    describe('/orders?page=1&limit=0', () => {
      it('should GET exactly 0 order', done => {
        chai
          .request(app)
          .get('/orders?page=1&limit=0')
          .end((err, res) => {
            res.should.have.status(200)
            res.body.should.be.a('array')
            res.body.length.should.be.eql(0)
            done()
          })
      })
    })

    describe('/orders', () => {
      it('should GET at most 10 orders by default, with absent of page and limit req.query', done => {
        chai
          .request(app)
          .get('/orders')
          .end((err, res) => {
            res.should.have.status(200)
            res.body.should.be.a('array')
            res.body.length.should.be.lte(10)
            done()
          })
      })
    })

    describe('/orders?page=1.6', () => {
      it('should fail while non-integer values are used for req.query.page', done => {
        chai
          .request(app)
          .get('/orders?page=1.6')
          .end((err, res) => {
            res.should.have.status(400)
            res.body.should.be.a('object')
            res.body.should.have.property('error')
            done()
          })
      })
    })

    describe('/orders?limit=2.6', () => {
      it('should fail while non-integer values are used for req.query.limit', done => {
        chai
          .request(app)
          .get('/orders?limit=2.6')
          .end((err, res) => {
            res.should.have.status(400)
            res.body.should.be.a('object')
            res.body.should.have.property('error')
            done()
          })
      })
    })
  })

  describe('PATCH', () => {
    describe('/orders/:id', () => {
      it('should PATCH the status of an order successfully', done => {
        let order = {
          origin: ['52.5160', '13.3779'],
          destination: ['52.5206', '13.3862'],
        }
        let updates = {
          status: 'TAKEN',
        }

        chai
          .request(app)
          .post('/orders')
          .send(order)
          .end((err, res) => {
            let id = res.body.id
            chai
              .request(app)
              .patch(`/orders/${id}`)
              .send(updates)
              .end((err, res) => {
                res.should.have.status(200)
                res.body.should.be.a('object')
                res.body.should.have.property('status').eql('SUCCESS')
                done()
              })
          })
      })

      it('should fail when PATCH for non-exist orders', done => {
        let updates = {
          status: "TAKEN",
        }

        chai
          .request(app)
          .patch('/orders/-1')
          .send(updates)
          .end((err, res) => {
            res.should.have.status(400)
            res.body.should.be.a('object')
            res.body.should.have.property('error')
            done()
          })
      })

      it('should fail due to race condition', done => {
        let order = {
          origin: ['52.5160', '13.3779'],
          destination: ['52.5206', '13.3862'],
        }
        let updates = {
          status: 'TAKEN',
        }

        chai
          .request(app)
          .post('/orders')
          .send(order)
          .end((err, res) => {
            let id = res.body.id
            console.log('1st patch, /orders/'+id)
            chai
              .request(app)
              .patch(`/orders/${id}`)
              .send(updates)
              .end((err, res) => {
                console.log(res.body)
                chai
                  .request(app)
                  .patch(`/orders/${id}`)
                  .send(updates)
                  .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.be.a('object')
                    res.body.should.have.property('error')
                    done()
                  })
              })
          })
      })
    })
  })
})
