## Configuration
### google map API key
- update environment variable `GOOGLE_MAP_APIKEY` in `docker-compose.yml`
  

## Lift the app
- execute `start.sh` to boot everything it needs (assume docker runtime and docker-compose are installed already)
  ```
  ./start.sh
  ```
  wait for a few seconds (mysql init), then the API should be available on `localhost:8080`

## Acknowledge
### thanks in adavnce for your time. please do let me know if there's any problem on boot it up via eedkevin@gmail.com