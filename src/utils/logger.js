const winston = require('winston')

const nodeEnv = process.env.NODE_ENV

const logger = winston.createLogger({
  format: winston.format.combine(
    winston.format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss',
    }),
    winston.format.json()
  ),
  transports: [new winston.transports.Console({ silent: nodeEnv === 'test' })],
})

module.exports = exports = logger
