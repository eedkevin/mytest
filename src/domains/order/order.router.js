const router = require('express').Router()
const cors = require('cors')
const handler = require('./order.handler')
const { placeOrderValidator, listOrderValidator, takeOrderValidator } = require('./order.validator')

const corsOptions = {
  origin: true, // reflect the request origin
  methods: 'GET,HEAD,PATCH,POST', // allowed methods
  preflightContinue: false, // pass the CORS preflight response to the next handler
  optionsSuccessStatus: 204,
}

router.use(cors(corsOptions))
router.get('/', listOrderValidator(), handler.listOrders)
router.post('/', placeOrderValidator(), handler.placeOrder)
router.patch('/:id', takeOrderValidator(), handler.takeOrder)

module.exports = exports = router
