module.exports = exports = {
  status: {
    UNASSIGNED: 'UNASSIGNED',
    TAKEN: 'TAKEN',
  },
  distance: {
    NOT_AVAILABEL: -1,
  },
}
