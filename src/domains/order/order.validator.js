const { check, validationResult } = require('express-validator')
const constant = require('./order.constant')

function placeOrderValidator() {
  return [
    check('origin')
      .isArray()
      .withMessage('must be an array')
      .custom(value => {
        if (value.length !== 2) throw new Error('must have exactly two elements')
        else if (typeof value[0] !== 'string' || typeof value[1] !== 'string')
          throw new Error('must be exactly two strings')
        else return true
      }),
    check('destination')
      .isArray()
      .withMessage('must be an array')
      .custom(value => {
        if (value.length !== 2) throw new Error('must have exactly two elements')
        else if (typeof value[0] !== 'string' || typeof value[1] !== 'string')
          throw new Error('must be exactly two strings')
        else return true
      }),
  ]
}

function listOrderValidator() {
  return [
    check('page')
      .optional()
      .isInt()
      .withMessage('must be integer'),
    check('limit')
      .optional()
      .isInt()
      .withMessage('must be integer'),
  ]
}

function takeOrderValidator() {
  return [
    check('status')
      .isString()
      .withMessage('must be string')
      .isIn(Object.values(constant.status))
      .withMessage(`must in ${constant.status}`),
  ]
}

module.exports = exports = {
  placeOrderValidator,
  listOrderValidator,
  takeOrderValidator,
}
