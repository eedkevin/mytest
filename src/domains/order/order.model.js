const { mysql } = require('../../common')
const { sequelize, Sequelize } = mysql
const { status, distance } = require('./order.constant')

const Order = sequelize.define(
  'order',
  {
    id: {
      type: Sequelize.BIGINT(20),
      primaryKey: true,
      autoIncrement: true,
    },
    origin: Sequelize.JSON,
    destination: Sequelize.JSON,
    distance: {
      type: Sequelize.INTEGER,
      defaultValue: distance.NOT_AVAILABEL,
    },
    is_deleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    }
  },
  {
    tableName: 'order',
    timestamps: false,
  }
)

const OrderStatus = sequelize.define(
  'order_status',
  {
    id: {
      type: Sequelize.BIGINT(20),
      primaryKey: true,
      autoIncrement: true,
    },
    order_id: Sequelize.BIGINT(20),
    status: {
      type: Sequelize.STRING,
      defaultValue: status.UNASSIGNED,
    },
  },
  {
    tableName: 'order_status',
    timestamps: false,
  }
)

// relationships
Order.hasOne(OrderStatus, {
  foreignKey: 'order_id',
  targetKey: 'id',
  onDelete: 'CASCADE',
  onUpdate: 'CASCADE',
})
OrderStatus.belongsTo(Order, {
  foreignKey: 'order_id',
  targetKey: 'id',
  onDelete: 'CASCADE',
  onUpdate: 'CASCADE',
})

module.exports = exports = {
  Order,
  OrderStatus,
  sequelize,
}
