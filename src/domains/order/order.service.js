const logger = require('../../utils/logger')
const dao = require('./order.dao')
const map = require('../../common/map')
const constant = require('./order.constant')

async function listOrders({ page, limit }) {
  let orders = await dao.listOrders({ page, limit })
  return orders
}

async function takeOrder(id, { status }) {
  let exists = await dao.orderExists(id)
  if (!exists) {
    return {
      success: false,
      message: 'order does not exist',
    }
  } else {
    let success = await dao.updateOrder(id, { status })
    if (success) {
      return {
        success: true,
        message: 'success',
      }
    } else {
      return {
        success: false,
        message: 'the order has already been taken by someone else',
      }
    }
  }
}

async function placeOrder({ origin, destination }) {
  let dist = await getDistance({ origin, destination })
  let { id, distance, status } = await dao.createOrder({ origin, destination, distance: dist })
  return { id, distance, status }
}

async function getDistance({ origin, destination }) {
  let distance = constant.distance.NOT_AVAILABEL
  try {
    let res = await map.distance({ origin, destination })

    if (checkResponse(res)) {
      distance = getClosestDistance(res)
      logger.info(
        `found distance[${distance}] from map API for origin[${origin.lat},${origin.lng}] destination[${destination.lat},${destination.lng}]`
      )
    } else {
      logger.warn(
        `haven't found distance from map API for origin[${origin.lat},${origin.lng}] destinationp${destination.lat},${destination.lng}]. using default distance[${distance}]`
      )
    }
  } catch (e) {
    logger.error(
      `request to map API for origin[${origin.lat},${origin.lng}] destination[${destination.lat},${destination.lng}] failed. using default distance[${distance}]`
    )
  }
  return distance
}

function checkResponse(res) {
  let { json } = res
  if (json.status !== 'OK') return false // response status is not OK

  let { rows } = json
  if (rows.length < 1) return false // response doesn't contain any useful row

  // check if any useful element in rows
  let count = 0
  for (let row of rows) {
    let { elements } = row
    // TODO: can be buggy since it's possibly to have more than 1 element, to be improve
    if (elements[0].status !== 'OK') count++
  }
  if (count === rows.length) return false // all elements are not useful

  return true
}

function getClosestDistance(res) {
  let distance = Number.MAX_SAFE_INTEGER
  for (let row of res.json.rows) {
    // TODO: can be buggy since it's possibly to have more thant 1 element, to be improve
    let d = row.elements[0].distance.value
    distance = d < distance ? d : distance
  }
  return distance
}

module.exports = exports = {
  listOrders,
  takeOrder,
  placeOrder,
}
