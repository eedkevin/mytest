const logger = require('../../utils/logger')
const { Order, OrderStatus, sequelize } = require('./order.model')

async function listOrders({ page, limit }) {
  let offset = (page - 1) * limit // page starts from 1, rather than 0
  logger.info(`listing orders - offset ${offset}, limit ${limit}`)

  let orders = await Order.findAll({
    attributes: ['id', 'distance'],
    offset: offset,
    limit: limit,
    include: [
      {
        model: OrderStatus,
        attributes: ['status'],
      },
    ],
    raw: true,
  })
  return flattenOrderStatus(orders)
}

async function createOrder({ origin, destination, distance }) {
  // make the following opertaions transactional
  let transaction = await sequelize.transaction({ autocommit: false })

  let newOrder = await Order.create({
    origin: origin,
    destination: destination,
    distance: distance,
  }, { transaction })

  let orderStatus = await OrderStatus.create({
    order_id: newOrder.id,
  }, { transaction })

  await transaction.commit()

  let { status } = orderStatus.dataValues
  return { ...newOrder.dataValues, status }
}

async function updateOrder(id, { status }) {
  let [updated] = await OrderStatus.update({ status: status }, { where: { order_id: id } })

  // mysql#update will return the number of records being affected,
  // thus only if the order really being updated by mysql will be treated as a success operation.
  // otherwise, it fails due to race condition with other requests
  if (updated === 1) return true
  else return false
}

async function orderExists(id) {
  let count = await Order.count({ where: { id: id } })
  return count == 0? false: true  // use the blurry equal in case mysql driver returns string presentation
}

function flattenOrderStatus(orders) {
  return orders.map(order => {
    order.status = order['order_status.status']
    delete order['order_status.status']
    return order
  })
}

module.exports = exports = {
  listOrders,
  createOrder,
  updateOrder,
  orderExists,
}
