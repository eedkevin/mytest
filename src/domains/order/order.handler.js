const httpstatus = require('http-status')
const { validationResult } = require('express-validator')
const logger = require('../../utils/logger')

const service = require('./order.service')
const dao = require('./order.dao')

async function listOrders(req, res) {
  try {
    let err = validationResult(req)
    if (!err.isEmpty()) {
      errMsg = `${err.array()[0].location}.${err.array()[0].param} ${err.array()[0].msg}`
      return res.status(httpstatus.BAD_REQUEST).json({ error: errMsg })
    }

    let { page = 1, limit = 10 } = req.query
    page = parseInt(page)
    limit = parseInt(limit)
    let orders = await service.listOrders({ page, limit })
    res.status(httpstatus.OK).json(orders)
  } catch (e) {
    logger.error(e.message)
    res.status(httpstatus.INTERNAL_SERVER_ERROR).json({ error: 'Service Internal Error' })
  }
}

async function placeOrder(req, res) {
  try {
    let err = validationResult(req)
    if (!err.isEmpty()) {
      errMsg = `${err.array()[0].location}.${err.array()[0].param} ${err.array()[0].msg}`
      return res.status(httpstatus.BAD_REQUEST).json({ error: errMsg })
    }

    const [oriLat, oriLng] = req.body.origin
    const [destLat, destLng] = req.body.destination
    const origin = { lat: oriLat, lng: oriLng }
    const destination = { lat: destLat, lng: destLng }

    let order = await service.placeOrder({ origin, destination })
    res.status(httpstatus.OK).json(order)
  } catch (e) {
    console.log(e)
    logger.error(e.message)
    res.status(httpstatus.INTERNAL_SERVER_ERROR).json({ error: 'Service Internal Error' })
  }
}

async function takeOrder(req, res) {
  try {
    let err = validationResult(req)
    if (!err.isEmpty()) {
      errMsg = `${err.array()[0].location}.${err.array()[0].param} ${err.array()[0].msg}`
      return res.status(httpstatus.BAD_REQUEST).json({ error: errMsg })
    }

    const { id } = req.params
    const { status } = req.body
    let { success, message } = await service.takeOrder(id, { status })
    if (success) res.status(httpstatus.OK).json({ status: 'SUCCESS' })
    else res.status(httpstatus.BAD_REQUEST).json({ error: message })
  } catch (e) {
    logger.error(e.message)
    res.status(httpstatus.INTERNAL_SERVER_ERROR).json({ error: 'Service Internal Error' })
  }
}

module.exports = exports = {
  listOrders,
  placeOrder,
  takeOrder,
}
