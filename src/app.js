require('dotenv').config()
const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const morgan = require('morgan')
const helmet = require('helmet')
const logger = require('./utils/logger')
const routers = require('./routers.js')

const app = express()

app.use(helmet())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

if (process.env.NODE_ENV !== 'test') {
  app.use(morgan('combined'))
  logger.info(`server is running on ${process.env.NODE_ENV || 'dev'} mode`)
}

app.use('/orders', routers.orderRouter)

module.exports = app
