const googlemap = require('@google/maps')

const mapRegion = process.env.GOOGLE_MAP_REGION || 'hk'
const client = googlemap.createClient({
  key: process.env.GOOGLE_MAP_APIKEY,
  Promise: Promise,
})

async function distance({ origin, destination }) {
  let res = await client
    .distanceMatrix({
      origins: `${origin.lat},${origin.lng}`,
      destinations: `${destination.lat},${destination.lng}`,
      units: 'metric',
      mode: 'driving',
      region: mapRegion,
    })
    .asPromise()

  if (res.status !== 200) throw new Error('Request failed')
  else return res
}

module.exports = exports = {
  distance,
}
