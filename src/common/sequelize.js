const Sequelize = require('sequelize')

const db = process.env.MYSQL_DB,
  user = process.env.MYSQL_USER,
  pwd = process.env.MYSQL_PWD,
  host = process.env.MYSQL_HOST,
  connPoolSize = parseInt(process.env.MYSQL_CONN_POOL_SIZE),
  nodeEnv = process.env.NODE_ENV

const sequelize = new Sequelize(db, user, pwd, {
  dialect: 'mysql',
  host: host,
  pool: {
    max: connPoolSize,
    min: 1,
    acquire: 30000,
    idle: 10000,
  },
  logging: nodeEnv === 'test' ? false : console.log,
})

module.exports = exports = {
  sequelize,
  Sequelize,
}
