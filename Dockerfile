FROM node:10.16-alpine

WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install --prod
COPY . .

EXPOSE 8080
CMD ["node", "./bin/www"]